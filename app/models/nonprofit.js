// app/models/charity.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NonprofitSchema = new Schema ({
	name: 			String,
	email: 			String,
	nonprofit_name: String,
	ein: 			String,
	text_color: 	String,
	instagram_url: 	String,
	twitter_url: 	String,
	facebook_url: 	String,
	npo_cause: 		String,
	npo_blurb: 		String,
	problems: 		String,
	url: 			String 	
	// background_img: { data: Buffer, contentType: String}
});

module.exports = mongoose.model('Nonprofit', NonprofitSchema);