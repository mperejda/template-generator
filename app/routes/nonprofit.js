// Depencencies ==================================================
var express = require('express');
var router = express.Router();


var Nonprofit = require('../models/nonprofit.js');


// Routes =======================================================

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('API is being accessed.');
	next();
});

// Nonprofit API
router.route('/')

	// create nonprofit
	.post(function(req, res) {
		console.log('POST /nonprofits');
		console.log(req.body)
		Nonprofit.create(req.body, function(err, nonprofit) {
			if (err)
				res.send(err);
			res.json("Successsfully created a new Nonprofit!");
		});
	})

	.get(function(req,res) {
		Nonprofit.find(function(err, nonprofits) {
			if (err)
				res.send(err);
			console.log('GET /nonprofits')
			res.json(nonprofits);
		});
	});

router.route('/:np_name')

	// get nonprofit by id
	.get(function(req, res) {
		console.log("GET /nonprofits")
		console.log(req.params.np_name);
		Nonprofit.find({
			url: req.params.np_name
		}, function(err, nonprofit) {
			if (err)
				res.send(err);
			res.json(nonprofit);
		});
	})

		// update nonprofit
	.put(function(req, res) {
		Nonprofit.findById(req.body.url, function(req, res) {
			if (err)
				res.send(err);

			// update nonprofit here

			res.json({ message: "Successsfully updated nonprofit!"});
		});
	})

	// delete nonprofit
	.delete(function(req, res) {
		// remove nonprofit here
	});

// Preview
router.route('/preview')
	.post(function (req, res) {
		console.log('Previewing')
		console.log(req.body)
		res.render('../public/preview', {json: req.body})
	})

module.exports = router;
