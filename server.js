// server.js

//
// SETUP ==================================================================================
require('dotenv').load();
var express = require('express');
var mongoose  = require('mongoose');			// mongoose for MongoDB
var bodyParser = require('body-parser');		// pull info from HTML POST requests
var fs = require('fs');							// for processing image uploads
var app = express(); 							// define our app using express
var port = process.env.PORT || 8080;  			// set our port

// CONFIG =================================================================================
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost/test');			// connect to MongDB
app.use(bodyParser.urlencoded( {extended: true} ));
app.use(bodyParser.json());


var db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', function(callback) {
	console.log('Connected to MongoDB at ' + db.host + ':' + db.port);
});

// ROUTES ================================================================================

var router = require('./app/routes/nonprofit.js');

// all routes will be prefixed with /nonprofits
app.use('/nonprofits', router);

// LOAD PAGE ==========================================================================
app.use(express.static('public'))

app.set('view engine', 'jade');
app.get('/', function(req, res) {
	res.render('../public/index')		// load a single file (angular will handle page changes on the fron end)
});

// Nonprofit Create Page
app.get('/create', function (req, res) {
	res.render('../public/create')
})

// START SERVER =======================================================================
app.listen(port);
console.log("Listening at localhost:" + port);
